/* Done */
"Done" = "Listo";

/* Title for an error message */
"informal_exclamation" = "¡Ay!";



/* Message for an unexpected error while scanning. */
"unexpected_scan_error_message" = "Ocurrió  un error inesperado. Espera un momento antes de volver a escanear.";

/* Message for an unexpected error . */
"unexpected_error_message" = "Ocurrió un error inesperado.";

/* Cancel */
"Cancel" = "Cancelar";

/* Title of error message regarding invalid content */
"Invalid Content" = "Contenido inválido";

/* Title- No Internet Connection */
"No Internet Connection" = "No hay conexión de Internet";

/* Message letting the user know he/she is offline and can't view content until he/she is back online */
"no_internet_connection_scan" = "Parece que no estás conectado a Internet. Si descubres que estás desconectado, respira hondo y relájate. Luego, verifica tu conexión a Internet, conéctate y vuelve a escanear.";

/* Message letting the user know he/she is offline  */
"no_internet_connection" = "Parece que no estás conectado a Internet.";

/* Title of a message letting the user know that the content they've scanned has expired */
"Link No Longer Active" = "El enlace ya no está activo";

/* Message letting the user know that the image they've scanned contains expired content. */
"link_no_longer_active_message" = "Error al buscar contenido. Este enlace puede haber expirado o ya no es válido.";

/* Title of a message letting the user know that their session has expired */
"user_authentication_session_expired_title" = "La sesión ha finalizado";

/* Message letting the user know that their session has expired */
"user_authentication_session_expired_message" = "La sesión ha finalizado. Por favor, reinicia la sesión.";

/* Title of a message letting the user know that their session has expired */
"user_authentication_signed_out_title" = "Signed out";

/* Message letting the user know that their session has expired */
"user_authentication_signed_out_message" = "You have been signed out.";

/* Title for an error message */
"Phone Call" = "Llamada telefónica";

/* Message explaining the user cannot make a phone call with their device. */
"phonePresenter_noPhoneCapabilityAlert_Body" = "Lo sentimos. Escaneaste contenido que marca un número, pero este dispositivo no puede hacer llamadas telefónicas.";

/* Message explaining the user cannot send a SMS with their device. */
"smsPresenter_noSMSCapabilityAlert_Body" = "We're so sorry. You scanned content that sends a SMS message, but this device can't send SMS messages.";

/* Title of message letting the user know that we don't support their image */
"You're Ahead Of Us!" = "¡Nos superaste!";

/* Message informs the user that we can't display the payoff for a QR code */
"captureViewController_message_unableToReadQRCode" = "Aún no podemos leer este código QR.  Nuestro equipo está trabajando para incluir este tipo de código. ¡Vuelve pronto para ver si ya lo logramos!";

/* Title for error messages */
"global_errorMessage_title" = "¡Ay!";


/* Title for a screen prompting the user to allow the application to access the address book so it can save personal contact details (business card) */
"Contact Found" = "Hemos encontrado un contacto";

/* Title for a screen prompting the user to change their privacy settings to allow access to the address book */
"contactsPresenter_previouslyDeclinedAlert_title" = "¿Habilitar los contactos?";

/* Title for a screen prompting the user to change their privacy settings to allow access to the address book */
"contactsPresenter_previouslyDeclinedAlertNoAction_title" = "Habilita los contactos";

/* Message- informs the user that the application will need access to the address book so it can save contact personal details (business card) */
"contactsPresenter_previouslyDeclinedAlertNoAction_body" = "Escaneaste una tarjeta de contacto, pero se ha deshabilitado el acceso a los contactos. Abre la aplicación Configuración, elige Privacidad > Contactos, y luego habilita LinkReader.";

/* Message- informs the user that the application will need access to the address book so it can save contact personal details (business card) */
"contactsPresenter_preAskAlert_body" = "Escaneaste una tarjeta de contacto y necesitaremos acceso a tu libreta de direcciones para guardarla";

/* Message- informs the user that the application will need access to the calendar so it can save the event */
"eventPresenter_preAskAlert_body" = "Escaneaste una tarjeta de evento y necesitaremos acceso a tu calendario para guardarla";

/* Message- informs the user they have previously denied calendar access, and to change if they want to */
"eventPresenter_previouslyDeclinedAlert_body" = "Escaneaste una tarjeta de evento, pero se ha deshabilitado el acceso al calendario. ¿Te gustaría cambiar tu configuración de privacidad?";

"eventPresenter_previouslyDeclinedAlertNoAction_body" = "Escaneaste una tarjeta de evento, pero se ha deshabilitado el acceso al Calendario. Abre la aplicación Configuración, elige Privacidad > Calendarios, y luego habilita LinkReader.";

/* Message- informs the user that the application will need access to the address book so it can save contact personal details (business card) */
"contactsPresenter_previouslyDeclinedAlert_body" = "Escaneaste una tarjeta de contacto, pero se ha deshabilitado el acceso a los Contactos. ¿Te gustaría cambiar tu configuración de privacidad?";

/* Title for a screen prompting the user to change their privacy settings to allow access to the calendar */
"calendarPresenter_previouslyDeclinedAlert_title" = "¿Habilitar el calendario?";

/* Settings Title for the 'Settings' screen */
"Settings" = "Configuración";

/* Title for a screen prompting the user to allow the application to access the calendar so it can save an event */
"Calendar Event Found" = "Hemos encontrado un evento para el calendario";

/* Decline request */
"global_alertButton_decline" = "No, gracias";

/* Error message, should not be user facing */
"DNT_encodedDataInvalidFormat" = "Encoded data is not properly formatted";

/* Message displayed when a video cannot be played */
"youTubePayoff_videoUnplayable" = "¡Ay!...El video no se pudo reproducir.\nPulsa el ícono para verlo en el navegador";

/* Message displayed when a image cannot be displayed */
"imagePayoff_imageUnviewable" = "Lo sentimos... La imagen no se puede ver.\nPulsa el ícono para verlo en el navegador";

/* Message displayed when an unsecure web payoff title cannot be displayed */
"title_not_found" = "El título de la página no fue encontrado";

/* (No Commment) */
"invalid_link_content" = "No se puede abrir el enlace porque el contenido es inválido.";

/* Title - A message informs the user that he must setup an e-mail account before send an email*/
"no_email_configured_title" = "No hay cuentas de correo";

/* Message- informs the user that he must setup an e-mail account before send an email*/
"no_email_configured_messsage" = "Configure una cuenta de correo para enviar correos electrónicos.";

/* Title for a server timeout error */
"server_timeout_title" = "Interrupción en la red";

/* Body for a server timeout error */
"server_timeout_body" = "La solicitud del servidor agotó el tiempo de espera. Revise su conexión o vuelva a intentar en un rato.";

/*Sign in error title */
"sign_in_error_title" = "Correo o contraseña incorrectos";

/*Sign in error message */
"sign_in_error_message" = "Inténtalo de nuevo";

/*Sign in title */
"sign_in_title" = "Iniciar Sesión";

/*close button*/
"close_button" = "Cerrar";
