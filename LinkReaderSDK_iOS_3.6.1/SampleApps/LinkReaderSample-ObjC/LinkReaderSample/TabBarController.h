//
//  TabBarController.h
//  LinkReaderSample
//
//  Created by Fernando Fernandes on 8/16/16.
//  Copyright © 2016 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarControllerTest: UITabBarController <UITabBarControllerDelegate>

@end
