//
//  TabBarController.m
//  LinkReaderSample
//
//  Created by Fernando Fernandes on 8/16/16.
//  Copyright © 2016 HP. All rights reserved.
//

#import "TabBarController.h"
#import "ViewControllerDefaultUX.h"
#import "ViewControllerCustomUX.h"

@implementation TabBarControllerTest

-(void)viewDidLoad{
    [self setDelegate:self];
}

- (void)tabBarController: (UITabBarController *) tabBarController didSelectViewController: (UIViewController *) viewController {
    
    ViewControllerDefaultUX *vcDefault = (ViewControllerDefaultUX *)tabBarController.viewControllers[0];
    ViewControllerCustomUX *vcCustom = (ViewControllerCustomUX *)tabBarController.viewControllers[1];
    
    // When clicking on the "Default UX" tab, stop the camera session in the "Custom UX" tab.
    if ([viewController isKindOfClass:[ViewControllerDefaultUX class]]) {
        [vcCustom.lrCaptureManager stopSession];
        [vcCustom unsetDelegates];
        vcCustom.cameraContainerView.hidden = true;
        
        [vcDefault setup];
        
    } else {
        [vcCustom setDelegates];
    }
}

@end
