//
//  ViewControllerCustomUX.h
//  LinkReaderAdvancedSample
//
//  Created by Live Paper Pairing on 11/24/2015.
//  Copyright (c) 2015 HP. All rights reserved.
//

#import <UIKit/UIKit.h>
@import LinkReaderKit;

@interface ViewControllerCustomUX : UIViewController

@property (weak, nonatomic) LRCaptureManager *lrCaptureManager;
@property (strong, nonatomic) IBOutlet UIView *cameraContainerView;

- (void)setDelegates;
- (void)unsetDelegates;

@end

