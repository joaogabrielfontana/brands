//
//  ViewControllerCustomUX.m
//  LinkReaderCustomUXSample
//
//  Copyright (c) 2015 HP. All rights reserved.
//

#import "ViewControllerCustomUX.h"
@import LinkReaderKit;

#define kDisabledColor [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1]
#define kEnabledColor [UIColor colorWithRed:17/255.0 green:130/255.0 blue:203/255.0 alpha:1]

static const int kAuthNetworkErrorAlert = 700;
static const int kAuthErrorAlert = 701;
static const int kScanningNetworkErrorAlert = 702;
static const int kPayoffDetailsAlert = 800;
static const int kPayoffDoneAlert = 801;
static const int kPayoffErrorAlert = 802;
static NSString *const kTestClientID = @"ClientID";

@interface ViewControllerCustomUX () <LRCaptureDelegate, LRDetectionDelegate, UIAlertViewDelegate, LRPresenterDelegate>

@property (weak, nonatomic) IBOutlet UILabel *authErrorLabel;
@property (weak, nonatomic) IBOutlet UIButton *startScanningButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic) id<LRPayoff> currentPayoff;
@property (nonatomic) LRPresenter * presenter;

@end

@implementation ViewControllerCustomUX

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.activityIndicator startAnimating];
    self.startScanningButton.enabled = NO;
    self.startScanningButton.backgroundColor = kDisabledColor;
    
    // 1. Pass your credentials to get authorized.
    [[LRManager sharedManager] authorizeWithClientID:kTestClientID secret:nil success:^{
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden = YES;
        self.startScanningButton.enabled = YES;
        self.startScanningButton.backgroundColor = kEnabledColor;
        [self setDelegates];
        
    } failure:^(NSError *error) {
        // Authentication or Network Error
        [self displayAuthErrorToUser:error];
    }];
}

// 2. Set self as delegate to receive important callbacks.
- (void)setDelegates {
    [[LRDetection sharedInstance] setDelegate:self];
    _lrCaptureManager = [LRCaptureManager sharedManager];
    _lrCaptureManager.delegate = self;
}

- (void)unsetDelegates {
    [[LRDetection sharedInstance] setDelegate:nil];
    _lrCaptureManager.delegate = nil;
}

- (IBAction)launchLinkReaderSDK:(id)sender {
    
    // 3. Add the video layer...
    if ([_lrCaptureManager startSession]) {
        NSLog(@"Video capture setup... success.");
        self.previewLayer = [_lrCaptureManager previewLayer];
        [self.previewLayer setFrame:self.view.bounds];
        [self.cameraContainerView.layer addSublayer:self.previewLayer];
    }
    
    self.cameraContainerView.hidden = false;
    
    // ... and start scanning.
    [self startScanning];
}

-(void)readerError:(NSError *)error {
    NSLog(@"Error: %@", error);
    NSString *title = [error userInfo][@"title"];
    NSString *message = [error userInfo][NSLocalizedDescriptionKey];
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

- (void)presentPayoffData:(id<LRPayoff>)payoff {
    self.currentPayoff = payoff;
    NSString * dataToPresent;
    if ([payoff isKindOfClass:[LRWebPayoff class]]) {
        dataToPresent = [(LRWebPayoff*)payoff url];
    }else if ([payoff isKindOfClass:[LRLayoutPayoff class]]) {
        dataToPresent = @"Rich payoff (LRLayoutPayoff)";
    }else{
        dataToPresent = [NSString stringWithFormat:@"Payoff: %@",[payoff displayString]];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Payoff Info" message:dataToPresent delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alert.tag = kPayoffDetailsAlert;
    [alert show];
}

- (void)presentPayoff {
    if (!self.presenter) {
        self.presenter = [LRPresenter new];
        self.presenter.delegate = self;
    }
    if ([self.presenter canPresentPayoff:self.currentPayoff]) {
        [self.presenter presentPayoff:self.currentPayoff viewController:self];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to present payoff" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag = kPayoffErrorAlert;
        [alert show];
    }
}
    
    
#pragma mark - UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == kAuthNetworkErrorAlert) {
        if (buttonIndex == 1) {
            // When the user clicks on Retry...
            [self.activityIndicator startAnimating];
            [[LRManager sharedManager] authorizeWithClientID:kTestClientID secret:nil success:^{
                [self.activityIndicator stopAnimating];
                self.startScanningButton.enabled = YES;
                self.startScanningButton.backgroundColor = kEnabledColor;
            } failure:^(NSError *error) {
                // Authentication or Network Error
                [self displayAuthErrorToUser:error];
            }];
        } else {
            [self showAuthorizationErrorMessage];
        }
    }else if(alertView.tag == kAuthErrorAlert){
        [self showAuthorizationErrorMessage];
    }else if(alertView.tag == kScanningNetworkErrorAlert ||
             alertView.tag == kPayoffDoneAlert ||
             alertView.tag == kPayoffErrorAlert
             ){
        [self startScanning];
    }else if(alertView.tag == kPayoffDetailsAlert){
        [self startScanning];
    }
}

#pragma mark - LRDetectionDelegate Methods
//a trigger has been detected
- (void)didFindTrigger:(LRTriggerType)triggerType {
    switch (triggerType) {
        case LRWatermark:
            NSLog(@"watermark");
            break;
            
        case LRQRCode:
            NSLog(@"qrcode");
            break;
        default: break;
    }
}

//a payoff linked to a read trigger has been retrieved
- (void)didFindPayoff:(id<LRPayoff>)payoff {
    [self presentPayoffData:payoff];
}

// 4. If something goes wrong, we'll tell you.
- (void)errorOnPayoffResolving:(NSError *)error {
    
    // Resolving errors mean that there was a problem retrieving the content.
    // For example: the content server is unreachable and/or the Internet
    // connection is offline.
    [self displayLRDetectionError: error tag:kScanningNetworkErrorAlert];
}

- (void)errorOnPayoffParsing:(NSError *)error {
    
    // Parsing errors mean that there was a problem with the content itself.
    // For example: the content was successfully retrieved, but it's somehow
    // defective (may contain typos, invalid character, etc).
    [self displayLRDetectionError: error tag:kPayoffErrorAlert];
}

- (void)displayLRDetectionError:(NSError *)error tag:(int) tag{
    
    NSString *title = @"LR Detection Error";
    NSString *message = error.localizedDescription;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
    alertView.tag = tag;
    [alertView show];
}

#pragma mark - LRPresenterDelegate Methods
    
-(void)presentationDidDismiss{
    [self startScanning];
}
    
    
#pragma mark - LRCaptureDelegate Methods

- (void)cameraFailedError:(NSError *)error {
    [self readerError:error];
    NSLog(@"There was an error with the camera session");
}

- (void)didChangeFromState:(LRCaptureState)fromState toState:(LRCaptureState)toState {
    switch (toState) {
        case LRCameraNotAvailable:
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"There was an error with the capture session" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            break;
        case LRCameraStopped:
            NSLog(@"Camera stopped");
            break;
        case LRCameraRunning:
            NSLog(@"Camera is running");
            break;
        case LRScannerRunning:
            NSLog(@"Scanner is running");
            break;
        default:
            break;
    }
}

#pragma mark - Helper Methods

- (void)startScanning {
    if ([[LRManager sharedManager] isAuthorized]) {
        NSError *error;
        [[LRCaptureManager sharedManager] startScanning: &error];
        if (error) {
            NSLog(@"An error occurred when scanning was started: %@", error);
            [self readerError:error];
        }else{
            [self initVideoPreview];
        }
    } else {
        NSLog(@"The App is not authorized to use the LinkReaderKit services");
    }
}


- (void)displayAuthErrorToUser:(NSError *)error{
    NSString *message;
    NSString *title;
    
    if ([error code] == LRAuthorizationErrorNetworkError) {
        message = [NSString stringWithFormat:@"There is a problem connecting with the server. error: %@",error.localizedDescription ];
        title = @"Network Error";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: @"Retry", nil];
        alertView.tag = kAuthNetworkErrorAlert;
        [alertView show];
        [self.activityIndicator stopAnimating];
        
    } else {
        if ([error code] == LRAuthorizationErrorApiKeyInvalid) {
            message = [NSString stringWithFormat:@"There was an authentication error: %@",error.localizedDescription ];
            title = @"Auth Error";
            
        } else {
            message = [NSString stringWithFormat:@"An error occurred. error: %@",error.localizedDescription ];
            title = @"Error";
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alertView.tag = kAuthErrorAlert;
        [alertView show];
        [self.activityIndicator stopAnimating];
    }
}

- (void)showAuthorizationErrorMessage{
    self.authErrorLabel.hidden = NO;
    self.startScanningButton.hidden = YES;
    self.activityIndicator.hidden = YES;
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [self initVideoPreview];
    
    self.previewLayer.frame = CGRectMake(0, 0, size.width, size.height);
}

- (void) initVideoPreview{
    if ([self.previewLayer.connection isVideoOrientationSupported]) {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        switch (orientation) {
            case UIDeviceOrientationPortrait:
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
            case UIDeviceOrientationLandscapeLeft:
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                break;
            case UIDeviceOrientationLandscapeRight:
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                break;
            default:
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
        }
    }
}

@end
