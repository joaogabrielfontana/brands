//
//  ViewControllerDefaultUX.m
//  LinkReaderSample
//
//  Copyright (c) 2015 HP. All rights reserved.
//

#import "ViewControllerDefaultUX.h"
@import LinkReaderKit;

#define kDisabledColor [UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1]
#define kEnabledColor [UIColor colorWithRed:17/255.0 green:130/255.0 blue:203/255.0 alpha:1]

static const int kNetworkErrorAlert = 700;
static const int kAuthErrorAlert = 701;
static NSString *const kTestClientID = @"ClientID";

@interface ViewControllerDefaultUX () <EasyReadingDelegate, LRPresenterDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) EasyReadingViewController *reader;
@property (weak, nonatomic) IBOutlet UILabel *authErrorLabel;
@property (weak, nonatomic) IBOutlet UIButton *startScanningButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) LRPresenter * presenter;
@end

@implementation ViewControllerDefaultUX

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.activityIndicator startAnimating];
    self.startScanningButton.enabled = NO;
    self.startScanningButton.backgroundColor = kDisabledColor;
    [self setup];
}

// 1. Pass your credentials and a delegate to receive important callbacks, get a view controller.
- (void)setup {
    self.reader = [[EasyReadingViewController alloc] initWithClientID:kTestClientID secret:nil delegate:self success:^{
        [self.activityIndicator stopAnimating];
        self.startScanningButton.enabled = YES;
        self.startScanningButton.backgroundColor = kEnabledColor;
        self.activityIndicator.hidden = YES;
    } failure:^(NSError *error) {
        // Authentication or Network Error
        [self displayAuthErrorToUser:error];
    }];
}

- (IBAction)launchLinkReaderSDK:(id)sender {
    
    // 2. Set the frame (will probably be full-screen).
    self.reader.view.frame = self.view.bounds;
    
    // 3. Present the view controller.
    [self presentViewController:self.reader animated:YES completion:^{
        NSLog(@"Easy as π!");
    }];
}

#pragma mark - EasyReadingDelegate

// 4. If something goes wrong, we'll tell you.
-(void)readerError:(NSError *)error {
    NSString *title = @"Reader Error";
    NSString *message = [error localizedDescription];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.reader resumeScanning];
    }]];
    [self.reader presentViewController:alertController animated:YES completion:nil];
}
- (BOOL)handlePayoff:(id <LRPayoff>)payoff {
    if ([payoff isKindOfClass:[LRPayoffBase class]]) {
        LRPayoffBase *validatePayoff = (LRPayoffBase*)payoff;
        NSString *type = @"Unknown";
        if ([payoff isKindOfClass:[LRLayoutPayoff class]]) {
            type = @"Rich";
        } else if ([payoff isKindOfClass:[LRHtmlPayoff class]]) {
            type = @"Html";
        }else if ([payoff isKindOfClass:[LRWebPayoff class]]) {
            type = @"Web URL";
        }else if ([payoff isKindOfClass:[ARPayoff class]]) {
            type = @"AR";
        }else if ([payoff isKindOfClass:[LRCustomDataPayoff class]]) {
            type = @"Custom Data";
        }
        NSString *validity = validatePayoff.privateData[@"validity"];
        NSString *title = [NSString stringWithFormat:@"%@ Payoff Retrieved", type];
        NSString *message = [NSString stringWithFormat:@"The validity status of the scanned item is %@",  (validity) ? validity : @"Unknown"];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Present Payoff" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (!self.presenter) {
                self.presenter = [LRPresenter new];
                self.presenter.delegate = self;
            }
            [self.presenter presentPayoff:payoff viewController:self.reader];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Continue Scanning" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.reader resumeScanning];
        }]];
        [self.reader presentViewController:alertController animated:YES completion:nil];
        return YES;
        
    } else {
        return NO;
    }
}

#pragma mark LRPresenterDelegate

-(void)presentationDidDismiss {
    // Need to restart easy reading scanner
    [self.reader resumeScanning];
}

#pragma mark - Helper Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == kNetworkErrorAlert) {
        if (buttonIndex == 1) {
            // When the user clicks on Retry...
            [self.activityIndicator startAnimating];
            [self.reader reauthenticateWithSuccess:^{
                [self.activityIndicator stopAnimating];
                self.startScanningButton.enabled = YES;
                self.startScanningButton.backgroundColor = kEnabledColor;
            } failure:^(NSError *error) {
                // Authentication or Network Error
                [self displayAuthErrorToUser:error];
            }];
        } else {
            [self showAuthorizationErrorMessage];
        }
    } else if(alertView.tag == kAuthErrorAlert){
        [self showAuthorizationErrorMessage];
    } else{
        [self.reader resumeScanning];
    }
}

- (void)displayAuthErrorToUser:(NSError *)error{
    NSString *message;
    NSString *title;
    
    if ([error code] == LRAuthorizationErrorNetworkError){
        message = [NSString stringWithFormat:@"There is a problem connecting with the server. error: %@",error.localizedDescription ];
        title = @"Network Error";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: @"Retry", nil];
        alertView.tag = kNetworkErrorAlert;
        [alertView show];
        [self.activityIndicator stopAnimating];
    }else{
        if ([error code] == LRAuthorizationErrorApiKeyInvalid) {
            message = [NSString stringWithFormat:@"There was an authentication error: %@",error.localizedDescription ];
            title = @"Auth Error";
        }else {
            message = [NSString stringWithFormat:@"An error occurred. error: %@",error.localizedDescription ];
            title = @"Error";
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alertView.tag = kAuthErrorAlert;
        [alertView show];
        [self.activityIndicator stopAnimating];
    }
}

- (void)showAuthorizationErrorMessage{
    self.authErrorLabel.hidden = NO;
    self.startScanningButton.hidden = YES;
    self.activityIndicator.hidden = YES;
}

@end
