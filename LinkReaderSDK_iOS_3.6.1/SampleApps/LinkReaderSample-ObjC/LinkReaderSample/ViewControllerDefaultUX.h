//
//  ViewControllerDefaultUX.h
//  LinkReaderSample
//
//  Created by Live Paper Pairing on 5/5/15.
//  Copyright (c) 2015 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerDefaultUX : UIViewController

- (void)setup;

@end
