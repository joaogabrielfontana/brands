//
//  ViewController.swift
//  LinkReaderSample
//
//  Created by Live Paper Pairing on 8/4/15.
//  Copyright (c) 2015 HP. All rights reserved.
//

import UIKit
import LinkReaderKit
import LinkReaderKit.LRPayoff
import LinkReaderKit.LRPresenter

class ViewController: UIViewController, EasyReadingDelegate, LRPresenterDelegate, UIAlertViewDelegate {

    let networkErrorAlert = 700
    let authErrorAlert = 701
    let disabledColor = UIColor(red: 196/255.0, green: 196/255.0, blue: 196/255.0, alpha: 1)
    let enabledColor = UIColor(red: 17/255.0 , green: 130/255.0, blue: 203/255.0, alpha: 1)
    let testClientID = Constants.testClientID
    var presenter:LRPresenter!
    
    var reader : EasyReadingViewController!
    @IBOutlet weak var authErrorLabel : UILabel!
    @IBOutlet weak var startScanningButton : UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.startScanningButton.isEnabled = false
        self.startScanningButton.backgroundColor = self.disabledColor
        
        // 1. Pass your credentials and a delegate to receive important callbacks, get a view controller
        
        self.reader = EasyReadingViewController(clientID: testClientID, secret: nil, delegate: self, success: {
            self.activityIndicator.stopAnimating()
            self.startScanningButton.isEnabled = true
            self.startScanningButton.backgroundColor = self.enabledColor
        }, failure: { (error) in
            self.displayAuthErrorToUser(error)
        })
    }
    
    @IBAction func launchLinkReaderSDK(_ button: UIButton) {
        // 2. Set the frame (will probably be full-screen)
        self.reader.view.frame = self.view.bounds
        
        // 3. Present the view controller
        self.present(self.reader, animated: true) { () -> Void in
            print("Easy as π!")
        }
    }
    
    func displayAuthErrorToUser(_ error : Error?) {
        // Handle authentication errors!!! ... Below code is just a placeholder, your app must handle these appropiately
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        if let authError = error as NSError? {
            switch authError.code {
            case LRAuthorizationError.networkError.rawValue:
                alertController.message = "There is a problem connecting with the server. error: " + authError.localizedDescription
                alertController.title = "Network Error"
                alertController.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (action) in
                    self.activityIndicator.startAnimating()
                    self.reader.reauthenticate(success: { () -> Void in
                        self.activityIndicator.stopAnimating()
                        self.startScanningButton.isEnabled = true
                        self.startScanningButton.backgroundColor = self.enabledColor
                    }, failure: { (error : Error?) -> Void in
                        // Authentication or Network Error
                        self.displayAuthErrorToUser(error)
                    })
                }))
            case LRAuthorizationError.apiKeyInvalid.rawValue:
                alertController.message = "The API key used was not valid: " + authError.localizedDescription
                alertController.title = "Auth Error"
            default:
                alertController.message = "There was an authentication error: " + authError.localizedDescription
                alertController.title = "Error"
            }
        }else{
            alertController.message = "There was an authentication error"
            alertController.title = "Error"
        }
        self.present(alertController, animated: true, completion: nil)
        self.activityIndicator.stopAnimating()
    }
    
    func showAuthorizationErrorMessage() {
        self.authErrorLabel.isHidden = false
        self.startScanningButton.isHidden = true
    }

    // Mark : EasyReadingDelegate methods
    
    func readerError(_ error: Error!) {
        let nsError  = error as NSError
        // Handle reader errors!!! ... Below code is just a placeholder, your app must handle these appropiately
        var errorCode = ""
        if (nsError.domain == LRPayoffResolverErrorDomain) {
            switch (nsError.code) {
            case LRPayoffResolverError.serverError.rawValue:
                errorCode = "LRPayoffResolverErrorServerError"
            case LRPayoffResolverError.payloadNotFound.rawValue:
                errorCode = "LRPayoffResolverErrorPayloadNotFound"
            case LRPayoffResolverError.payloadOutOfRange.rawValue:
                errorCode = "LRPayoffResolverErrorPayloadOutOfRange"
            case LRPayoffResolverError.badResolveRequest.rawValue:
                errorCode = "LRPayoffResolverErrorBadResolveRequest"
            case LRPayoffResolverError.connectionError.rawValue:
                errorCode = "LRPayoffResolverErrorConnectionError"
            case LRPayoffResolverError.requestCancelled.rawValue:
                errorCode = "LRPayoffResolverErrorRequestCancelled"
            case LRPayoffResolverError.networkConnectionLost.rawValue:
                errorCode = "LRPayoffResolverErrorNetworkConnectionLost"
            case LRPayoffResolverError.networkRequestTimedOut.rawValue:
                errorCode = "LRPayoffResolverErrorNetworkRequestTimedOut"
            case LRPayoffResolverError.notAuthorized.rawValue:
                errorCode = "LRPayoffResolverErrorNotAuthorized"
            default:
                errorCode = "Unexpected Code"
            }
        }
        else if (nsError.domain == LRPayoffErrorDomain) {
            switch (nsError.code) {
            case LRPayoffError.payoffNotPresentInData.rawValue:
                errorCode = "LRPayoffErrorPayoffNotPresentInData"
            case LRPayoffError.invalidPayoffData.rawValue:
                errorCode = "LRPayoffErrorInvalidPayoffData"
            case LRPayoffError.missingOrUnknownPayoffType.rawValue:
                errorCode = "LRPayoffErrorMissingOrUnknownPayoffType"
            case LRPayoffError.invalidPayoffFormat.rawValue:
                errorCode = "LRPayoffErrorInvalidPayoffFormat"
            default:
                errorCode = "Unexpected Code"
            }
        }
        
        let message = "Error while detecting mark: \(nsError.domain) - \(errorCode)"
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.reader.resumeScanning()
        }))
        self.reader.present(alert, animated: true, completion: nil)
    }
    
    func handle(_ payoff:LRPayoff) -> Bool {
        if (payoff is LRPayoffBase) {
            let validatePayoff: LRPayoffBase = payoff as! LRPayoffBase
            var payoffType: String = "Unknown"
            if (payoff is LRLayoutPayoff) {
                payoffType = "Rich";
            } else if (payoff is LRHtmlPayoff) {
                payoffType = "Html";
            } else if (payoff is LRWebPayoff) {
                payoffType = "Web URL";
            } else if (payoff is ARPayoff) {
                payoffType = "AR";
            } else if (payoff is LRCustomDataPayoff) {
                payoffType = "Custom Data";
            }

            let validity:String? = validatePayoff.privateData?["validity"] as? String
            let title = String(format:"%@ Payoff Retrieved", payoffType)
            let message = String(format:"The scanned item is %@", validity == nil ? "Unknown" : (validity as String?)!)
            let alertController = UIAlertController(title:title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Present Payoff", style: .default, handler: { (action) in
                if (self.presenter == nil) {
                    self.presenter = LRPresenter()
                    self.presenter.delegate = self
                }
                self.presenter.present(payoff, viewController: self.reader)
            }))
            alertController.addAction(UIAlertAction(title: "Continue Scanning", style: .default, handler: { (action) in
                self.reader.resumeScanning()
            }))
            self.reader.present(alertController, animated: true, completion: nil)
            return true;
        } else {
            return false;
        }
    }

    func presentationDidDismiss() {
        self.reader.resumeScanning()
    }
}

