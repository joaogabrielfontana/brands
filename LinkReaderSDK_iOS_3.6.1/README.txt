#LinkReader SDK

 Version | Release Date
-------- | -------------
 3.6.1   | 02/27/2019
 3.6.0   | 12/10/2018
 3.5.0   | 09/17/2018
 3.4.1   | 05/18/2018
 3.4.0   | 04/18/2018
 3.3.0   | 03/27/2018
 3.2.0   | 01/11/2018
 3.1.1   | 12/07/2017
 3.1.0   | 11/07/2017
 2.2.0   | 07/10/2017
 2.1.0   | 03/27/2017
 2.0.0   | 09/12/2016
 1.2.0   | 06/29/2016
 1.1.0   | 12/22/2015
 1.0.1   | 08/21/2015
 1.0     | 07/14/2015

The iOS SDK consists of a framework build for iOS8+

##Zip Content
Framework - contains the SDK library to be included on a project
Docs - generated appledoc on html format
Launch Screen Badge - splash screen badge to be included on the app that used the SDK
SampleApps - a SDK sample exemplifying a Default UI and Custom UI implementations
Localization - contains localized strings used for SDK messages

##Sample Description
The Sample provided in the Zip contains two ways in how to use the LinkReader SDK.
The easy way is to create a EasyReadingViewController that contains a default implementation of the scan control and UI. The use and creation of this ViewController can be found inside the ViewControllerDefaultUX class provided in the Sample.
A more customized option is to use direct methods from the LRManager, LRDetection and LRCaptureManager classes, as exemplified in the ViewControllerCustomUX implementation in the sample, allowing a developer to control and create custom experiences based on the detected responses.

##Terms and Conditions
Please review the latest HP Terms and Conditions for using the LinkReader SDK.  A link to the HP Terms and Conditions can be found on https://www.linkcreationstudio.com/developer/scan-tools/sdk/ios-linkreader/

##Open source attributions
The LinkReader SDK open source page is hosted on https://www.linkcreationstudio.com/developer/scan-tools/sdk/ios-linkreader/open-source-attributions.html

##Documentation & Installation
For set up and getting started, please visit https://www.linkcreationstudio.com/developer/scan-tools/sdk/ios-linkreader/

