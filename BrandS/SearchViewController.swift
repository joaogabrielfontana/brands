//
//  FirstViewController.swift
//  BrandS
//
//  Created by João Gabriel on 18/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit
import LinkReaderKit
import LinkReaderKit.LRPayoff
import LinkReaderKit.LRPresenter

class SearchViewController: UIViewController, EasyReadingDelegate, LRPresenterDelegate, UIAlertViewDelegate {
    let networkErrorAlert = 700
    let authErrorAlert = 701
    let testClientID = Constants.testClientID
    let secret = Constants.secret
    var reader: EasyReadingViewController!


    @IBOutlet weak var cameraButton: UIBarButtonItem! {
        didSet {
            self.cameraButton.tintColor = CustomColors.lightBlue
        }
    }
    
    func readerError(_ error: Error!) {
        
    }

    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.placeholder = String.localize(key: Localizables.searchBarPlaceholder)
            let textField = self.searchBar.value(forKey: "searchField") as? UITextField
            textField?.font = textField?.font?.withSize(15)
        }
    }
    
    @IBOutlet weak var instructionsLabel: UILabel! {
        didSet {
            self.instructionsLabel.text = String.localize(key: Localizables.instructions)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search"
        self.tabBarController?.tabBarItem.badgeColor = CustomColors.lightBlue
        self.navigationItem.rightBarButtonItem?.image = UIImage(named: "camera")
        self.navigationController?.navigationBar.backgroundColor = CustomColors.lightBlue
    }
    
    @IBAction func launchReader(_ sender: Any) {
        self.reader = EasyReadingViewController(clientID: testClientID, secret: secret, delegate: self, success: {
            self.reader.view.frame = self.view.bounds
            self.present(self.reader, animated: true) { () -> Void in
                print("Easy as π!")
            }
        }, failure: { (error) in
            print("erro")
        })
    }
}

