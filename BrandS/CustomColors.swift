//
//  CustomColors.swift
//  BrandS
//
//  Created by João Gabriel on 18/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit

struct CustomColors {
    static var lightBlue = UIColor(hex: "#ADD8E6")
}
