//
//  LocalizableStrings.swift
//  BrandS
//
//  Created by João Gabriel on 18/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit

struct Localizables {
    static let searchBarPlaceholder = "search_bar_placeholder"
    static let instructions = "instructions"
}
