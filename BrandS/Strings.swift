//
//  StringExtension.swift
//  BrandS
//
//  Created by João Gabriel on 18/04/19.
//  Copyright © 2019 João Gabriel. All rights reserved.
//

import UIKit


extension String {
    static func localize(key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
